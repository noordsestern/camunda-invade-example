*** Settings ***
Library    CamundaLibrary   ${CAMUNDA_HOST}

*** Variables ***
${CAMUNDA_HOST}    http://localhost:8080

*** Tasks ***
New Emporer wants to invade somewhere
    [Documentation]    Start invasion plans
    start process    ExpandRome    variables=${{ {'order' : 'where do we invade?'} }}

Decide how to best to expand rome
    [Documentation]    Choose destination for new emporer's next vacation
    ${order_of_emporer}    Fetch workload    ChooseDestination
    Run keyword if    ${order_of_emporer}    Decide direction and give order

Invade Gaul
    [Documentation]    Invade Gaul, if order received
    ${order_to_invade_gaul}    Fetch workload    InvadeGaul
    Run keyword if    ${order_to_invade_gaul}    invade gaul and report to emporer

Invade Persia
    [Documentation]    Invade Persia, if order received
    ${order_to_invade_persia}    Fetch workload    InvadePersia
    Run keyword if    ${order_to_invade_persia}    invade persia and report to emporer

*** Keywords ***
Decide direction and give order
    ${dice}    Evaluate    random.randint(1,101)    modules=random
    Run keyword if     ${dice} < 50    send army north
    ...     ELSE    send army south

Send army north
    ${decision}    Create Dictionary    north=${True}
    complete task    ${decision}

Send army south
    ${decision}    Create Dictionary    north=${False}
    complete task   ${decision}

Invade gaul and report to emporer
    log    THIS IS GAUL!!!    WARN
    complete task

invade persia and report to emporer
    log    THIS IS PERSIA!!!    WARN
    complete task
