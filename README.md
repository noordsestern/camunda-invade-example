# Demo for CamundaLibrary
This is a demo for `robotframework-camunda`. The goal is to give an introduction to task automation with 
[Robot Framework](https://robotframework.org) in general and 
[CamundaLibrary](https://gitlab.com/postadress/robotframework/robotframework-camunda) in particular.

The uses the model used by [Niall Deehan](https://github.com/NPDeehan) and [Professor Giddygreaves](https://en.wikipedia.org/wiki/Harris%27s_hawk) 
in their Tutorial about [Camunda in Distributed Systems](https://youtu.be/l6pMXr8Jf6k).

The original project from this tutorial is based on Java and Javascript and can be found on 
[Github](https://github.com/camunda-consulting/code/tree/master/snippets/InvadeWorker).

## Preperation

1. Clone this repository: https://gitlab.com/noordsestern/camunda-invade-example.git
1. Install Python 3
1. In the project folder run `pip install -r install/requirements.txt`
1. Run Camunda BPM Engine, for example with docker by launching: `docker run -d --name camunda -p 8080:8080 camunda/camunda-bpm-platform:latest`
1. Upload Niall's invasion modell, for example by using robot framework: 
`robot -d logs upload_invasion_model.robot`
   
## Run process
Run command in project folder: `robot -d logs run_simple_invasion.robot`

The command will launch a full invasion of either Gaul or Persia, depending on fate. Reports of the invasion will be saved in `logs` folder.

## Explanation

The tasks only cover Service Tasks of the modell, yet, because CamundaLibrary does not support events, yet. Covered activities from the model are colored:
![Service Tasks that are covered in this simple example](docs/img/run_simple_invasion.png)

The robot file `run_simple_invasion.robot` consists of several tasks and is called a *Task Suite*. The first task starts the process, the other 3 are external service tasks.
You can verify also in the `logs/log.html` how the invasion went (Persia or Gaul).

If you are new to Robot Framework, investigate how you can create new *keywords* from existing ones in the `*** Keywords ***` section.

If you want to run a single task use the `-t <name of task>` option, for example: `robot -d logs -t "Invade Gaul" run_simple_invasion.robot`